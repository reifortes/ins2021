import sys, os, glob

def convert(value):
    return float(value.replace(',', '.'))


def processResults(path):
    files = glob.glob(f'{path}*-FUN.tsv')
    result = {}
    for file in files:
        solution = os.path.basename(file)[:-8]
        file = open(file, 'r')
        line = file.readline().strip().split()
        result[solution] = convert(line[0])
    return result


def searchWinner(algs, keys):
    algsDict = { k: algs[k] for k in keys }
    if len(algsDict) == 0: return None
    winner = min(algsDict, key=algsDict.get)
    return winner


def getConfigLine(configFile, strategy):
    file = open(configFile, 'r')
    lines = file.readlines()
    line = [ l for l in lines if f'PSO_{strategy}' in l ]
    return line[0]


def saveConfigLines(configLine, winner, tuningConfig, selfeature, outFile):
    newline = configLine
    configs = tuningConfig.split('/')
    config = [ x for x in configs if x.startswith(winner[-2:]) ]
    newline = newline.replace('<TUNING_CONFIG>', config[0])
    newline = newline.replace('<MO>', selfeature)
    outFile.write(newline)


if __name__ == '__main__':
    print('Parameters: ' + str(sys.argv))
    print("Inicio")
    home = sys.argv[1]
    resultPath = sys.argv[2]
    configFile = home+sys.argv[3]
    tuningConfig = sys.argv[4]
    outFile = home+sys.argv[5]
    if len(sys.argv) > 6: strategies = sys.argv[6].strip().split(';')
    else: strategies = [ 'HR', 'FWLS', 'STREAM']
    algs = processResults(resultPath)
    outFile = open(outFile, 'w')
    for strategy in strategies:
        print('- Strategy: %s' % (strategy))
        keys1 = [k for k in algs if strategy in k]
        for selfeature in [ "-all", "_G_0.05_R_0.95_Cum" ]:
            print('---- Feature Selection: %s' % ('No' if 'all' in selfeature else 'Yes'))
            keys2 = [ k for k in keys1 if selfeature in k ]
            winner = searchWinner(algs, keys2)
            if not winner: continue
            print('------------ Winner: %s' % (winner))
            configLine = getConfigLine(configFile, strategy)
            saveConfigLines(configLine, winner, tuningConfig, selfeature, outFile)
    outFile.close()
    print("Fim")
