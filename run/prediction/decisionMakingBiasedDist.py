import os, sys, csv
from scipy.spatial import distance

def readUsers(fileName):
    global users
    users = {}
    tsv_reader = csv.reader(open(fileName, 'rt'), delimiter='\t')
    for aux, data in enumerate(tsv_reader):
        if 'nan' in data: continue
        users[data[0]] = [ float(x) for x in data[1:] ]


def computeDistance(p1, p2):
    return abs(distance.euclidean(p1, p2))


def selectSolution(arqSol, userWeights):
    global objWeights
    selD = float("inf")
    selLine = 0
    for l, line in enumerate(arqSol):
        numbers = [ abs(float(x)) for x in line.strip().split(' ') ]
        sumNumbers = sum(numbers)
        weights = [ x / sumNumbers for x in numbers]
        d = computeDistance(userWeights, weights)
        if d < selD:
            selD = d
            selLine = l
    return selLine


def processFile(file, userWeights):
    global homeDir, users, outfileName
    fileName = '%s/%s' % (homeDir, file)
    arqSol = open(fileName.replace('-VAR.tsv', '-FUN.tsv'), 'r')
    sol = selectSolution(arqSol, userWeights)
    arqSol.close()
    arqWeight = open(fileName, 'r')
    weights = []
    for i, line in enumerate(arqWeight):
        if i == sol:
            weights = line.strip()
            return weights


def processFolder():
    print("Processing folder.")
    global homeDir, users, outfileName
    files = [ s for s in os.listdir(homeDir) if ('-VAR.tsv' in s) ]
    for file in files:
        outfile = open('%s/%s' % (homeDir, file.replace('VAR.tsv', outfileName + '.csv')), 'w')
        for user in users:
            weights = processFile(file, users[user])
            outfile.write('%s;%s\n' % (user, weights))
        outfile.close()


if __name__ == '__main__':
    print("Begin")
    homeDir     = sys.argv[1]
    userWeights = sys.argv[2]
    outfileName = sys.argv[3]
    readUsers(userWeights)
    processFolder()
    print("End")
