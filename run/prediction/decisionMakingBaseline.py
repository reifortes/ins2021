import os,sys


def selectSolution(arqSol):
    global objWeights, sumObjWeights
    selMean = float("-inf")
    selLine = 0
    for l, line in enumerate(arqSol):
        numbers = [ abs(float(x)) for x in line.strip().split(' ') ]
        mean = sum(n * w for n, w in zip(numbers, objWeights)) / sumObjWeights
        if mean > selMean:
            selMean = mean
            selLine = l
    return selLine


def processFile(file):
    print("- processing file: %s" % file)
    global homeDir, outfileName
    fileName = '%s%s' % (homeDir, file)
    arqSol = open(fileName.replace('-VAR.tsv', '-FUN.tsv'), 'r')
    sol = selectSolution(arqSol)
    arqSol.close()
    arqWeight = open(fileName, 'r')
    weights = []
    for i, line in enumerate(arqWeight):
        if i == sol:
            weights = line.strip()
            break
    arqWeight.close()
    outfile = open(fileName.replace('VAR.tsv', outfileName+'.csv'), 'w')
    outfile.write('%s\n' % (weights))
    outfile.close()

def processFolder():
    print("Processing folder.")
    global homeDir
    files = [ s for s in os.listdir(homeDir) if ('-VAR.tsv' in s) ]
    for file in files:
        processFile(file)


if __name__ == '__main__':
    print("Begin")
    homeDir     = sys.argv[1]
    objWeights  = [float(x) for x in sys.argv[2].split(";")]
    outfileName = sys.argv[3]

    sumObjWeights = sum(objWeights)
    processFolder()

    print("End")
