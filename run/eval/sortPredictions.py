import sys
sys.path.insert(0, '/Users/reifortes/Downloads/temp/execution11/run/')
sys.path.insert(0, '/home/terralab/reinaldo/execution11/run')
import os
import operator


def getAlgName(alg):
    config = []
    if 'PEH' in alg:
        # PEH strategy (baseline, it does not use metafeatures)
        if   'PEH-acc'  in alg: config.append('PEH-acc')
        elif 'PEH-div'  in alg: config.append('PEH-div')
        elif 'PEH-nov'  in alg: config.append('PEH-nov')
        elif 'PEH-mean' in alg: config.append('PEH-mean')
        # Metafeatures strategy
        if   'HR'     in alg: pass #config.append('HR')
        elif 'STREAM' in alg: return None
        elif 'FWLS'   in alg: return None
        # Feature Selection
        if   '-all' in alg: pass #config.append('Without-FS')
        else: return None
    elif 'PSO' in alg:
        config.append('SO')
        # Hibridization strategy
        if   'HR'     in alg: config.append('HR')
        elif 'STREAM' in alg: config.append('STREAM')
        elif 'FWLS'   in alg: config.append('FWLS')
        # Feature selection
        if   '-all' not in alg: config.append('FS')
    else:
        # Optimization strategy
        if   'D_true'  in alg: config.append('EPD')
        elif 'S_false' in alg and '-all' in alg: config.append('DDM')
        else: return None
        # Statistical test
        if   'S_true' in alg: config.append('Sig')
        # Hibridization strategy
        if   'HR'     in alg: config.append('HR')
        elif 'STREAM' in alg: config.append('STREAM')
        elif 'FWLS'   in alg: config.append('FWLS')
        # Feature selection
        if   '-all' not in alg: config.append('FS')
    return '_'.join(config)


class Prediction:
    user = None
    item = None
    value = None
    def __init__(self, userId, itemId, value):
        self.user = userId
        self.item = itemId
        self.value = value
    def getStr(self):
        return self.user + '\t' + self.item + '\t' + str(self.value)


def checkRecursive(d):
    global predictionPath
    checkDir(os.path.join(predictionPath, d))
    for root, dirs, files in os.walk(d):
        for dirname in dirs:
            checkDir(os.path.join(d, dirname))


def checkDir(d):
    global predictionPath, foldFilter, fileFilter
    if os.path.isdir(d) and foldFilter in d:
        print('=======\nProcessing Path: %s' % d)
        files = [ f for f in os.listdir(d) if ('.txt' in f and fileFilter in f) ]
        for f in files:
            print(' - File: %s' % f)
            processFile(d, f)


def readPredictionsFile(fileName):
    arq = open(fileName, 'r', encoding="utf-8")
    predictions = {}
    line = arq.readline().strip()
    while(len(line) != 0):
        values = line.split(',')
        (userId, itemId, value) = values[:3]
        prediction = Prediction(userId, itemId, float(value))
        if userId not in predictions:
            predictions[userId] = []
        predictions[userId].append(prediction)
        line = arq.readline().strip()
    arq.close()
    return predictions


def processFile(path, fileName):
    global predictionPath, outPutPath
    print('- File: %s' % filename)
    algName = getAlgName(fileName)
    if algName:
        predictions = readPredictionsFile('%s/%s' % (path, fileName))
        outPath = '%s%s/' % (outPutPath, path.replace(predictionPath, ''))
        if not os.path.exists(outPath): os.makedirs(outPath)
        out = open(f'{outPath}{algName}.txt', 'w', encoding="utf-8")
        for user in predictions:
            predictions[user].sort(key=operator.attrgetter("value"), reverse=True)
            for i in range(0, len(predictions[user])):
                out.write(predictions[user][i].getStr() + '\n')
        out.close()


if __name__ == '__main__':
    print("Begin")
    dataSet        = sys.argv[1]
    home           = sys.argv[2]
    predictionPath = '%s/%s/' % (dataSet, sys.argv[3])
    outPutPath     = '%s/%s/%s/' % (dataSet, home, sys.argv[4])
    fileFilter     = sys.argv[5]
    foldFilter     = sys.argv[6]
    dirList = os.listdir(predictionPath)
    dirList.sort()
    for filename in dirList:
        if os.path.isdir(os.path.join(predictionPath, filename)):
            checkRecursive(os.path.join(predictionPath, filename))
        elif filename.endswith('.txt'):
            processFile(predictionPath, filename)
    print("End")
