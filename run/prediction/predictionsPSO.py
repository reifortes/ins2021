import os,sys, glob


def getFeatureFile(alg):
    global scikitFiles
    for file in scikitFiles:
        if file.lower() in alg.lower():
            return f'{file}-test'
    return None


def readNotBiasedWeigths(file):
    weightsFile = open('%s%s' % (weightsPath, file), 'r')
    weights = [ float(x) for x in weightsFile.readline().strip().split(' ') ]
    weightsFile.close()
    return weights


def computeNotBiasedScore(features, weights):
    score = 0
    for f, feature in enumerate(features):
        score += float(feature.split(':')[1]) * weights[f]
    return score


def processPredictions(file):
    print("Processing: %s" % (file))
    global weightsPath, predictionDir, solutionFile, featuresPath, keysFileName
    weights = readNotBiasedWeigths(file)
    outFileName = os.path.basename(file).replace('.tsv', '')
    outfile = open('%s%s.txt' % (predictionDir, outFileName.replace('-VAR', '')), 'w')
    featuresFile = getFeatureFile(file)
    if featuresFile is None:
        print('- Features File not found.')
        return
    print('- Features File: %s' % featuresFile)
    featuresFile = open('%s%s.skl' % (featuresPath, featuresFile), 'r')
    keysFile = open('%s%s' % (featuresPath, keysFileName), 'r')
    for features, keys in zip(featuresFile, keysFile):
        features = features.strip().split()[1:]
        score = computeNotBiasedScore(features, weights)
        if score is None: continue
        outfile.write('%s,%.6f\n' % (keys.strip(), score))
    outfile.close()


if __name__ == '__main__':
    print("Inicio")
    homePath       = sys.argv[1]
    weightsPath    = homePath + sys.argv[2]
    solutionFile   = sys.argv[3]
    featuresPath   = homePath + sys.argv[4]
    keysFileName   = sys.argv[5]
    predictionDir  = homePath + sys.argv[6]
    try:
        if not os.path.exists(predictionDir): os.makedirs(predictionDir)
    except Exception:
        pass
    scikitFiles = glob.glob('%s%s-test.skl' % (featuresPath, solutionFile))
    scikitFiles = [ os.path.basename(s).replace('-test.skl', '') for s in scikitFiles ]
    files = [ s for s in os.listdir(weightsPath) if (solutionFile in s and '-VAR.tsv' in s) ]
    for file in files:
        processPredictions(file)
    print("Fim")
