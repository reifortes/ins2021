#!/usr/bin/env bash

NOW=$(date +"%Y-%m-%d_%H.%M.%S")
echo $NOW
START=$(date +%s)

echo ""
echo "Parameters:"
for i; do
     echo "- $i"
done
echo ""

DATASET=${1}
R1=${4}
R2=${5}
CORES=3
CORES_EVAL=4
CORES_MAIN=5
MAXMEM=30

RunFolder="run/search"
OutFolder="${DATASET}/out/pso/"
mkdir -p ${OutFolder}

TUNING_CONFIG="01;0;0/02;0;1/03;1;1/04;1;2"

#####
SEQ='1'
if [ "${2}" -le "${SEQ}" ] && [ "${SEQ}" -le "${3}" ]; then
    ExecFile="tuningPSO"
    echo ""
    echo "*****************************************************************"
    echo "${SEQ} - ${ExecFile}"
    TEMP_FILE=${DATASET}_${FOLD}_${R1}_${R2}_${ExecFile}.temp.exec
    rm -f ${TEMP_FILE}
    sed -e "s:<MEM>:${MAXMEM}:g" -e "s:<DATASET>:${DATASET}:g" -e "s:<FOLD>:Tuning:g" -e "s:<MO>:-all:g"               -e "s:<PORTION>:train:g" -e "s:<TIME_LIMIT>:7200000:g" -e "s:<TIME_SAVE>:0:g" -e "s:<TUNING_CONFIG>:${TUNING_CONFIG}:g" -e "s:<CORES_EVAL>:${CORES_EVAL}:g" -e "s:<CORES_MAIN>:${CORES_MAIN}:g" -e "s:<R1>:${R1}:g" -e "s:<R2>:${R2}:g" -e "s:<OUT>:${OutFolder}${SEQ}_:g" ${RunFolder}/${ExecFile}.txt >> ${TEMP_FILE}
    sed -e "s:<MEM>:${MAXMEM}:g" -e "s:<DATASET>:${DATASET}:g" -e "s:<FOLD>:Tuning:g" -e "s:<MO>:_G_0.05_R_0.95_Cum:g" -e "s:<PORTION>:train:g" -e "s:<TIME_LIMIT>:7200000:g" -e "s:<TIME_SAVE>:0:g" -e "s:<TUNING_CONFIG>:${TUNING_CONFIG}:g" -e "s:<CORES_EVAL>:${CORES_EVAL}:g" -e "s:<CORES_MAIN>:${CORES_MAIN}:g" -e "s:<R1>:${R1}:g" -e "s:<R2>:${R2}:g" -e "s:<OUT>:${OutFolder}${SEQ}_:g" ${RunFolder}/${ExecFile}.txt >> ${TEMP_FILE}
    python -u run/executeCommands.py ${TEMP_FILE} ${CORES} > ${OutFolder}${SEQ}_${ExecFile}.out
    rm -f ${TEMP_FILE}
fi

#####
SEQ='2'
if [ "${2}" -le "${SEQ}" ] && [ "${SEQ}" -le "${3}" ]; then
    ExecFile="tuningEvaluation"
    echo ""
    echo "*****************************************************************"
    echo "${SEQ} - ${ExecFile}"
    echo "- tuningEvaluation: processTunedWinnersPSO"
    python -u ${RunFolder}/processTunedWinnersPSO.py ${RunFolder}/ ${DATASET}/PSO/Tuning/R0/ tuningPSO.txt $TUNING_CONFIG search_Winners_pso_${DATASET}.txt >${OutFolder}${SEQ}_${ExecFile}_processTunedWinnersPSO.out
fi

#####
SEQ='3'
if [ "${2}" -le "${SEQ}" ] && [ "${SEQ}" -le "${3}" ]; then
    ExecFile="searchWinners"
    echo ""
    echo "*****************************************************************"
    echo "${SEQ} - ${ExecFile}"
    TEMP_FILE=${DATASET}_${ExecFile}.${SAMPLE}.${R1}.${R2}.temp.exec
    array_folds=("F1234-5" "F1235-4" "F1245-3" "F1345-2" "F2345-1")
    rm -f ${TEMP_FILE}
    for fold in "${array_folds[@]}"; do
        subFold=${fold:1:4}
        sed -e "s:<MEM>:${MAXMEM}:g" -e "s:<DATASET>:${DATASET}:g" -e "s:<FOLD>:${fold}:g" -e "s:Sample1234-sortedByRating_BIAS_1.txt:Sample${subFold}-sortedByRating_BIAS_1.txt:g" -e "s:<PORTION>:train:g" -e "s:<TIME_LIMIT>:18000000:g" -e "s:<TIME_SAVE>:0:g" -e "s:<CORES_EVAL>:${CORES_EVAL}:g" -e "s:<CORES_MAIN>:${CORES_MAIN}:g" -e "s:<R1>:${R1}:g" -e "s:<R2>:${R2}:g" -e "s:<OUT>:${OutFolder}${SEQ}_:g" ${RunFolder}/search_Winners_pso_${DATASET}.txt >> ${TEMP_FILE}
    done
    python -u run/executeCommands.py ${TEMP_FILE} ${CORES} >> ${OutFolder}${SEQ}_${ExecFile}-${SAMPLE}-SH.out
    rm -f ${TEMP_FILE}
fi

END=$(date +%s)
DIFF=$(($END - $START))
echo ""
echo ""
echo "Finished in $DIFF seconds"
NOW=$(date +"%D %T")
echo $NOW
