import sys

def convert(value):
    return float(value.replace(',', '.'))


def processQualityIndicators(file):
    result = {}
    file = open(file, 'r')
    header = file.readline().strip().split(';')
    col = [header.index(x) for x in header if x == 'IGD'][0]
    for line in file:
        line = line.strip().split(';')
        if '-seq_' in line[0] or '_C_0.3' in line[0] or '_C_0.5' in line[0]: continue
        result[line[0]] = convert(line[col])
    return result


def searchWinner(algs, keys):
    algsDict = { k: algs[k] for k in keys }
    if len(algsDict) == 0: return None
    winner = min(algsDict, key=algsDict.get)
    return winner


def getConfigLine(configFile, winner):
    file = open(configFile, 'r')
    search = winner.split('_')
    config = int(search[1])
    lines = file.readlines()
    return lines[config]


def saveConfigLines(repStrategy, repExtreme, repStatisticalTest, repConfidence, line, outFile):
    global repBD, repTimeLimit, repTimeSave, repOUT
    foldsConfig = [
        ('F2345-1', 'Sample2345.txt', '2345'),
        ('F1345-2', 'Sample1345.txt', '1345'),
        ('F1245-3', 'Sample1245.txt', '1245'),
        ('F1235-4', 'Sample1235.txt', '1235'),
        ('F1234-5', 'Sample1234.txt', '1234')
    ]
    for config in foldsConfig:
        newline = line
        newline = newline.replace('<BD>', repBD)
        newline = newline.replace('Scikit/Tuning/', 'Scikit/')
        newline = newline.replace('MO/TuningNSGA/',  'MO/<R>/%s/' % config[0])
        newline = newline.replace('"F"', '"%s"' % config[0])
        newline = newline.replace('TuningSample.txt', config[1])
        newline = newline.replace('<STRATEGY>', repStrategy)
        newline = newline.replace('<EXTREME>', repExtreme)
        newline = newline.replace('<STATISTICALTEST>', repStatisticalTest)
        newline = newline.replace('<CONFIDENCE>', repConfidence)
        newline = newline.replace('<FOLD_TRAIN>', config[2])
        newline = newline.replace('<TIME_LIMIT>', repTimeLimit)
        newline = newline.replace('<TIME_SAVE>', repTimeSave)
        newline = newline.replace('<OUT>', repOUT)
        newline = newline.replace('_Tuning', '_Final')
        newline = newline.replace('.out', '-%s.out' % config[0])
        outFile.write(newline)


if __name__ == '__main__':
    print('Parameters: ' + str(sys.argv))
    print("Begin")
    home = sys.argv[1]
    qiFile = sys.argv[2]
    configFile = home+sys.argv[3]
    outFile = home+sys.argv[4]
    repBD = sys.argv[5]
    repTimeLimit = sys.argv[6]
    repTimeSave = sys.argv[7]
    repOUT = sys.argv[8]
    if len(sys.argv) > 9: strategies = sys.argv[9].strip().split(';')
    else: strategies = [ 'HR', 'STREAM', 'FWLS']
    algs = processQualityIndicators(qiFile)
    outFile = open(outFile, 'w')
    for metafeature in strategies:
        print('- Metafeatues: %s' % ('No' if 'HR' in metafeature else 'Yes'))
        keys1 = [ k for k in algs if metafeature in k ]
        for selfeature in [ "-all-", "_G_0.05_R_0.95_Cum-" ]:
            print('---- Feature Selection: %s' % ('No' if 'all' in selfeature else 'Yes'))
            keys2 = [ k for k in keys1 if selfeature in k ]
            for extreme in [ "_D_false", "_D_true" ]:
                print('-------- Extreme Domination: %s' % ('No' if 'false' in extreme else 'Yes'))
                keys3 = [ k for k in keys2 if extreme in k ]
                for statisticalTest in [ "_S_false", "_S_true" ]:
                    keys4 = [k for k in keys3 if statisticalTest in k]
                    winner = searchWinner(algs, keys4)
                    if not winner: continue
                    print('------------ Statistical test: %s' % ('No' if 'false' in statisticalTest else 'Yes'))
                    print('------------ Winner: %s' % (winner))
                    configLine = getConfigLine(configFile, winner)
                    repExtreme = 'true' if 'true' in extreme else 'false'
                    repStatisticalTest = 'true' if 'true' in statisticalTest else 'false'
                    repConfidence = '0' if repStatisticalTest == 'false' else ('0.05' if '0.05' in winner else '0.1')
                    saveConfigLines('%s%strain' % (metafeature, selfeature), repExtreme, repStatisticalTest, repConfidence, configLine, outFile)
    outFile.close()
    print("End")
