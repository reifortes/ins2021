#!/usr/bin/env bash

NOW=$(date +"%D %T")
echo $NOW
START=$(date +%s)

echo ""
echo "Parameters:"
for i; do
     echo "- $i"
done
echo ""

R1=${4}
R2=${5}
CORES=20

RunFolder="run/eval"
OutFolder="${1}/out/eval/"
mkdir -p ${OutFolder}

#####
SEQ='1'
if [ "${2}" -le "${SEQ}" ] && [ "${SEQ}" -le "${3}" ]; then
    ExecFile="sortPredictions"
    echo ""
    echo "*****************************************************************"
    echo "${SEQ} - ${ExecFile}"
    rm -f ${ExecFile}.${1}.temp.exec
    for R in $(seq ${R1} ${R2}); do
        sed -e "s:<BD>:${1}:g" -e "s:<RUN>:${RunFolder}:g" -e "s:<OUT>:${OutFolder}:g" -e "s:<R>:R${R}:g" ${RunFolder}/${ExecFile}.txt >>${ExecFile}.${1}.temp.exec
    done
    python -u run/executeCommands.py ${ExecFile}.${1}.temp.exec ${CORES} >${OutFolder}${ExecFile}-R${R1}_${R2}.out
    rm -f ${ExecFile}.${1}.temp.exec
fi

#####
SEQ='2'
if [ "${2}" -le "${SEQ}" ] && [ "${SEQ}" -le "${3}" ]; then
    ExecFile="evaluationMetricsCalculator"
    echo ""
    echo "*****************************************************************"
    echo "${SEQ} - ${ExecFile}"
    rm -f ${ExecFile}.${1}.temp.exec
    for R in $(seq ${R1} ${R2}); do
        sed -e "s:<R>:R${R}:g" -e "s:<BD>:${1}:g" -e "s:<RUN>:${RunFolder}:g" -e "s:<OUT>:${OutFolder}:g" -e "s:<K>:5:g" ${RunFolder}/${ExecFile}.txt >>${ExecFile}.${1}.temp.exec
    done
    python -u run/executeCommands.py ${ExecFile}.${1}.temp.exec ${CORES} >${OutFolder}${ExecFile}-R${R1}_${R2}.out
    rm -f ${ExecFile}.${1}.temp.exec
fi

END=$(date +%s)
DIFF=$(($END - $START))
echo ""
echo ""
echo "Finished in $DIFF seconds"
NOW=$(date +"%D %T")
echo $NOW
