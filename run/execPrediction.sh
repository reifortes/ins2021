#!/usr/bin/env bash

NOW=$(date +"%D %T")
echo $NOW
START=$(date +%s)

echo ""
echo "Parameters:"
for i; do
     echo "- $i"
done
echo ""

#MO_FOLD=${4}
#TYPE=${5}
#MO_REP=${6}
#MO=${7}
R1=${4}
R2=${5}
RUN_BASELINE=${6}
CORES=20

#MO_NAME=${MO_FOLD}_${TYPE}_${MO_REP}

RunFolder="run/prediction/"
OutFolder="${1}/out/prediction/"
mkdir -p ${OutFolder}

for R in $(seq ${R1} ${R2}); do
    echo ""
    echo "*****************************************************************"
    echo "* Running ${R}"
    echo "*****************************************************************"
    echo ""
    NOW_R=$(date +"%D %T")
    echo "-- Beginning: $NOW_R"
    START_R=$(date +%s)
    #####
    SEQ='1'
    if [ "${2}" -le "${SEQ}" ] && [ "${SEQ}" -le "${3}" ]; then
        ExecFile="decisionMaking"
        echo ""
        echo "*****************************************************************"
        echo "${SEQ} - ${ExecFile}"
        rm -f ${1}.${ExecFile}.temp.all
        sed -e "s/<BD>/${1}/g" -e "s:<R>:R${R}:g" -e "s:<RUN>:${RunFolder}:g" -e "s:<OUT>:${OutFolder}:g" ${RunFolder}/${ExecFile}BiasedDist.txt >>${1}.${ExecFile}.temp.all
        if [ "${RUN_BASELINE}" -eq "1" ]; then
            sed -e "s/<BD>/${1}/g" -e "s:<R>:R${R}:g" -e "s:<RUN>:${RunFolder}:g" -e "s:<OUT>:${OutFolder}:g" ${RunFolder}/${ExecFile}Baseline.txt >>${1}.${ExecFile}.temp.all
        fi
        python -u run/executeCommands.py ${1}.${ExecFile}.temp.all ${CORES} >${OutFolder}${ExecFile}_${R}.out
        rm -f ${1}.${ExecFile}.temp.all
    fi
    #####
    SEQ='2'
    if [ "${2}" -le "${SEQ}" ] && [ "${SEQ}" -le "${3}" ]; then
        ExecFile="prediction"
        echo ""
        echo "*****************************************************************"
        echo "${SEQ} - prediction MO"
        rm -f ${1}.${ExecFile}.temp.all
        sed -e "s/<BD>/${1}/g" -e "s/<R>/R${R}/g" -e "s:<RUN>:${RunFolder}:g" -e "s:<OUT>:${OutFolder}:g" ${RunFolder}/${ExecFile}BiasedDist.txt >>${1}.${ExecFile}.temp.all
        if [ "${RUN_BASELINE}" -eq "1" ]; then
            sed -e "s/<BD>/${1}/g" -e "s/<R>/R${R}/g" -e "s:<RUN>:${RunFolder}:g" -e "s:<OUT>:${OutFolder}:g" ${RunFolder}/${ExecFile}Baseline.txt >>${1}.${ExecFile}.temp.all
        fi
        python -u run/executeCommands.py ${1}.${ExecFile}.temp.all ${CORES} >${OutFolder}${ExecFile}_MO_${R}.out
        rm -f ${1}.${ExecFile}.temp.all
    fi
    #####
    SEQ='3'
    if [ "${2}" -le "${SEQ}" ] && [ "${SEQ}" -le "${3}" ]; then
        ExecFile="prediction"
        echo ""
        echo "*****************************************************************"
        echo "${SEQ} - prediction PSO"
        rm -f ${1}.${ExecFile}.temp.all
        sed -e "s/<BD>/${1}/g" -e "s:<R>:R${R}:g" -e "s:<RUN>:${RunFolder}:g" -e "s:<OUT>:${OutFolder}:g" ${RunFolder}/${ExecFile}Baseline-pso.txt >>${1}.${ExecFile}.temp.all
        python -u run/executeCommands.py ${1}.${ExecFile}.temp.all ${CORES} >${OutFolder}${ExecFile}_PSO_${R}.out
        rm -f ${1}.${ExecFile}.temp.all
    fi
    END_R=$(date +%s)
    DIFF_R=$(($END_R - $START_R))
    echo ""
    echo "- Finished in $DIFF_R seconds"
    NOW_R=$(date +"%D %T")
    echo $NOW_R
    echo ""
done

END=$(date +%s)
DIFF=$(($END - $START))
echo ""
echo ""
echo "Finished in $DIFF seconds"
NOW=$(date +"%D %T")
echo $NOW
