import resource, gc
import numpy, math
from scipy.spatial import distance

def using(point=""):
    usage = resource.getrusage(resource.RUSAGE_SELF)
    print('%s: usertime = %f; systime = %f; mem= %.2f' % (point, usage[0], usage[1], (usage[2]*resource.getpagesize())/1000000.0))
    gc.collect()


#http://mundoeducacao.bol.uol.com.br/matematica/angulo-entre-dois-vetores.htm
def calcAngle(point1, point2):
    uv = 0
    u = 0
    v = 0
    for c1, c2 in zip(point1, point2):
        uv += c1 * c2
        u += c1 * c1
        v += c2 * c2
    u = math.sqrt(u)
    v = math.sqrt(v)
    if u*v == 0:
        return float("+inf")
    else:
        arco = uv / (u * v)
        if arco > 1: arco = 1
        if arco < -1: arco = -1
        return math.acos(arco)


# Point distances
def calcPointDist(point1, point2, name):
    if 'vector' in name.lower(): return calcAngle(point1, point2)
    elif 'cosine' in name.lower(): return distance.cosine(point1, point2)
    elif 'correlation' in name.lower():  return distance.correlation(numpy.array(point1, dtype=numpy.float32), numpy.array(point2, dtype=numpy.float32))
    #elif 'mahalanobis' in name.lower():
    #    if numpy.isnan(userBiasPoint).any(): return distance.mahalanobis(suppBiasPoint, point, None)
    #    else: return alfa * abs(distance.mahalanobis(userBiasPoint, point, None)) + (1 - alfa) * abs(distance.mahalanobis(suppBiasPoint, point, None))
    else: return distance.euclidean(point1, point2)


# Goal point distances
def calcDist(userBiasPoint, suppBiasPoint, point, alfa, name):
    if numpy.isnan(userBiasPoint).any(): return abs(calcPointDist(suppBiasPoint, point, name))
    else: return alfa * abs(calcPointDist(userBiasPoint, point, name)) + (1 - alfa) * abs(calcPointDist(suppBiasPoint, point, name))


