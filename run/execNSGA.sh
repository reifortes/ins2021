#!/usr/bin/env bash

NOW=$(date +"%D %T")
echo $NOW
START=$(date +%s)

echo ""
echo "Parameters:"
for i; do
     echo "- $i"
done
echo ""

BEGIN=${4}
END=${5}
CORES=20

RunFolder="run/search"
OutFolder="${1}/out/search/"
mkdir -p ${OutFolder}

#####
SEQ='1'
if [ "${2}" -le "${SEQ}" ] && [ "${SEQ}" -le "${3}" ]; then
    ExecFile="tuningNSGA"
    echo ""
    echo "*****************************************************************"
    echo "${SEQ} - ${ExecFile}"
    array_bool=("true" "false")
    array_conf=("0.05" "0.1")
    rm -f ${ExecFile}.temp.exec.all
    for ext in "${array_bool[@]}"; do
        sed -e "s/<BD>/${1}/g" -e "s:<OUT>:${OutFolder}:g" -e "s/<STRATEGY>/HR-all-train/g"                   -e "s/<EXTREME>/${ext}/g" -e "s/<STATISTICALTEST>/false/g" -e "s/<CONFIDENCE>/0/g"  -e "s/<FOLD_TRAIN>/2345/g" -e "s/<TIME_LIMIT>/7200000/g" -e "s/<TIME_SAVE>/0/g" ${RunFolder}/${ExecFile}.txt >>${ExecFile}.temp.exec.all
        sed -e "s/<BD>/${1}/g" -e "s:<OUT>:${OutFolder}:g" -e "s/<STRATEGY>/FWLS-all-train/g"                 -e "s/<EXTREME>/${ext}/g" -e "s/<STATISTICALTEST>/false/g" -e "s/<CONFIDENCE>/0/g"  -e "s/<FOLD_TRAIN>/2345/g" -e "s/<TIME_LIMIT>/7200000/g" -e "s/<TIME_SAVE>/0/g" ${RunFolder}/${ExecFile}.txt >>${ExecFile}.temp.exec.all
        sed -e "s/<BD>/${1}/g" -e "s:<OUT>:${OutFolder}:g" -e "s/<STRATEGY>/STREAM-all-train/g"               -e "s/<EXTREME>/${ext}/g" -e "s/<STATISTICALTEST>/false/g" -e "s/<CONFIDENCE>/0/g"  -e "s/<FOLD_TRAIN>/2345/g" -e "s/<TIME_LIMIT>/7200000/g" -e "s/<TIME_SAVE>/0/g" ${RunFolder}/${ExecFile}.txt >>${ExecFile}.temp.exec.all
        sed -e "s/<BD>/${1}/g" -e "s:<OUT>:${OutFolder}:g" -e "s/<STRATEGY>/HR_G_0.05_R_0.95_Cum-train/g"     -e "s/<EXTREME>/${ext}/g" -e "s/<STATISTICALTEST>/false/g" -e "s/<CONFIDENCE>/0/g"  -e "s/<FOLD_TRAIN>/2345/g" -e "s/<TIME_LIMIT>/7200000/g" -e "s/<TIME_SAVE>/0/g" ${RunFolder}/${ExecFile}.txt >>${ExecFile}.temp.exec.all
        sed -e "s/<BD>/${1}/g" -e "s:<OUT>:${OutFolder}:g" -e "s/<STRATEGY>/FWLS_G_0.05_R_0.95_Cum-train/g"   -e "s/<EXTREME>/${ext}/g" -e "s/<STATISTICALTEST>/false/g" -e "s/<CONFIDENCE>/0/g"  -e "s/<FOLD_TRAIN>/2345/g" -e "s/<TIME_LIMIT>/7200000/g" -e "s/<TIME_SAVE>/0/g" ${RunFolder}/${ExecFile}.txt >>${ExecFile}.temp.exec.all
        sed -e "s/<BD>/${1}/g" -e "s:<OUT>:${OutFolder}:g" -e "s/<STRATEGY>/STREAM_G_0.05_R_0.95_Cum-train/g" -e "s/<EXTREME>/${ext}/g" -e "s/<STATISTICALTEST>/false/g" -e "s/<CONFIDENCE>/0/g"  -e "s/<FOLD_TRAIN>/2345/g" -e "s/<TIME_LIMIT>/7200000/g" -e "s/<TIME_SAVE>/0/g" ${RunFolder}/${ExecFile}.txt >>${ExecFile}.temp.exec.all
        for ic in "${array_conf[@]}"; do
            sed -e "s/<BD>/${1}/g" -e "s:<OUT>:${OutFolder}:g" -e "s/<STRATEGY>/HR-all-train/g"                   -e "s/<EXTREME>/${ext}/g" -e "s/<STATISTICALTEST>/true/g" -e "s/<CONFIDENCE>/${ic}/g"  -e "s/<FOLD_TRAIN>/2345/g" -e "s/<TIME_LIMIT>/7200000/g" -e "s/<TIME_SAVE>/0/g" ${RunFolder}/${ExecFile}.txt >>${ExecFile}.temp.exec.all
            sed -e "s/<BD>/${1}/g" -e "s:<OUT>:${OutFolder}:g" -e "s/<STRATEGY>/FWLS-all-train/g"                 -e "s/<EXTREME>/${ext}/g" -e "s/<STATISTICALTEST>/true/g" -e "s/<CONFIDENCE>/${ic}/g"  -e "s/<FOLD_TRAIN>/2345/g" -e "s/<TIME_LIMIT>/7200000/g" -e "s/<TIME_SAVE>/0/g" ${RunFolder}/${ExecFile}.txt >>${ExecFile}.temp.exec.all
            sed -e "s/<BD>/${1}/g" -e "s:<OUT>:${OutFolder}:g" -e "s/<STRATEGY>/STREAM-all-train/g"               -e "s/<EXTREME>/${ext}/g" -e "s/<STATISTICALTEST>/true/g" -e "s/<CONFIDENCE>/${ic}/g"  -e "s/<FOLD_TRAIN>/2345/g" -e "s/<TIME_LIMIT>/7200000/g" -e "s/<TIME_SAVE>/0/g" ${RunFolder}/${ExecFile}.txt >>${ExecFile}.temp.exec.all
            sed -e "s/<BD>/${1}/g" -e "s:<OUT>:${OutFolder}:g" -e "s/<STRATEGY>/HR_G_0.05_R_0.95_Cum-train/g"     -e "s/<EXTREME>/${ext}/g" -e "s/<STATISTICALTEST>/true/g" -e "s/<CONFIDENCE>/${ic}/g"  -e "s/<FOLD_TRAIN>/2345/g" -e "s/<TIME_LIMIT>/7200000/g" -e "s/<TIME_SAVE>/0/g" ${RunFolder}/${ExecFile}.txt >>${ExecFile}.temp.exec.all
            sed -e "s/<BD>/${1}/g" -e "s:<OUT>:${OutFolder}:g" -e "s/<STRATEGY>/FWLS_G_0.05_R_0.95_Cum-train/g"   -e "s/<EXTREME>/${ext}/g" -e "s/<STATISTICALTEST>/true/g" -e "s/<CONFIDENCE>/${ic}/g"  -e "s/<FOLD_TRAIN>/2345/g" -e "s/<TIME_LIMIT>/7200000/g" -e "s/<TIME_SAVE>/0/g" ${RunFolder}/${ExecFile}.txt >>${ExecFile}.temp.exec.all
            sed -e "s/<BD>/${1}/g" -e "s:<OUT>:${OutFolder}:g" -e "s/<STRATEGY>/STREAM_G_0.05_R_0.95_Cum-train/g" -e "s/<EXTREME>/${ext}/g" -e "s/<STATISTICALTEST>/true/g" -e "s/<CONFIDENCE>/${ic}/g"  -e "s/<FOLD_TRAIN>/2345/g" -e "s/<TIME_LIMIT>/7200000/g" -e "s/<TIME_SAVE>/0/g" ${RunFolder}/${ExecFile}.txt >>${ExecFile}.temp.exec.all
        done
    done
    python -u run/executeCommands.py ${ExecFile}.temp.exec.all ${CORES} >${OutFolder}_${ExecFile}.out
    rm -f ${ExecFile}.temp.exec.all
fi

#####
SEQ='2'
if [ "${2}" -le "${SEQ}" ] && [ "${SEQ}" -le "${3}" ]; then
    ExecFile="tuningEvaluation"
    echo ""
    echo "*****************************************************************"
    echo "${SEQ} - ${ExecFile}"
    echo "- tuningEvaluation: qualityIndicatorsProcessing"
    python -u ${RunFolder}/qualityIndicatorsProcessing.py ${1} /MO/TuningNSGA/ Sample2345-sortedByRating_BIAS_1.txt tuningEval_IGD.csv >${OutFolder}${ExecFile}_QualityIndicatorsProcessing.out
    echo "- tuningEvaluation: processTunedWinnersAll"
    python -u ${RunFolder}/processTunedWinners.py ${RunFolder}/ ${1}/MO/TuningNSGA/tuningEval_IGD.csv tuningNSGA.txt NSGA-${1}_Tuning.txt ${1} 21600000 600000 ${OutFolder} >${OutFolder}${ExecFile}_processTunedWinners.out
fi

#####
SEQ='3'
if [ "${2}" -le "${SEQ}" ] && [ "${SEQ}" -le "${3}" ]; then
    for R in $(seq ${BEGIN} ${END}); do
        ExecFile="search"
        echo ""
        echo "*****************************************************************"
        echo "${SEQ} - ${ExecFile} - Running ${R}"
        NOW_R=$(date +"%D %T")
        echo "-- Beginning: $NOW_R"
        START_R=$(date +%s)
        mkdir -p ${1}/out/nsga
        sed -e "s:<R>:R${R}:g" -e "s:.out\n:_R-${R}.out\n:g" ${RunFolder}/NSGA-${1}_Tuning.txt >${RunFolder}/NSGA-${1}_${R}.txt
        python -u run/executeCommands.py ${RunFolder}/NSGA-${1}_${R}.txt ${CORES} >${OutFolder}${ExecFile}_${R}.out
        END_R=$(date +%s)
        DIFF_R=$(($END_R - $START_R))
        echo "- Finished in $DIFF_R seconds"
        NOW_R=$(date +"%D %T")
        echo $NOW_R
        echo ""
    done
fi

END=$(date +%s)
DIFF=$(($END - $START))
echo ""
echo ""
echo "Finished in $DIFF seconds"
NOW=$(date +"%D %T")
echo $NOW
