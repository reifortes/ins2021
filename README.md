# Fortes et al. (INS'2021)

Dataset used in **Fortes et al.**, *Individualized extreme dominance (IndED): A new preference-based method for multi-objective recommender systems.*. Information Sciences, Volume 572, 2021, Pages 558-573, ISSN 0020-0255. DOI: 10.5220/0006315406230633.

Datasets contain data processed from the original datasets, freely available on the Internet, as described in the article. Use licenses must follow the original data licenses.

## Datasets

Three datasets:

- Bookcrossing
- Jester
- ML1M

## Data structure

Two directories:

- **BD**: Contains user data files (ratings and preferences), partitioned into 5-fold cross validation, selected data for tuning and item attributes.

    - *groupUsers**: selected users for testing phases.
    - *Sample?* and *Sample????*: ratings samples form testing and training phases.
    - *Sample*BIAS*: the users' preferences on the optimized objectives. 
    - *TuningSample*: data selected for MO methods tuning.
    - *u.item.attributes*: items' content attributes.
    - *usersThreshold_empty*: an auxiliary empty file.
    
- **Scikit**: Contains files of processed features generated from the constituent algorithms and meta-features for all folds and for tuning, for the three strategies (HR, STREAM, and FWLS).

    - _\*-all-\*_: files containing all features.
    - _\*G_0.05__R_0.95_Cum\*_: files containing selected features.
    - _\*.features\*_: lists the features names.
    - _\*.merged\*_: processed files merging keys and features for the MO methods.
    - _\*.keys\*_: user/item ids.


## Running files:

The runnable files are in the run folder. Execution can be performed from shell scripts, in the following order (examples for Bookcrossing dataset):

#### 1. Tuning execution:
- nohup ./run/execNSGA.sh Bookcrossing 1 2 0 0 > Bookcrossing/out/execNSGA-1-2-0-0.out 2>&1&
- nohup ./run/execPSO.sh  Bookcrossing 1 2 0 0 > Bookcrossing/out/execPSO-1-2-0-0.out 2>&1&

#### 2. Search execution (for 15 replications):
- nohup ./run/execNSGA.sh Bookcrossing 3 3 1 15 > Bookcrossing/out/execNSGA-3-3-1-15.out 2>&1&
- nohup ./run/execPSO.sh  Bookcrossing 3 3 1 15 > Bookcrossing/out/execPSO-3-3-1-15.out 2>&1&

#### 3. Prediction execution (for 15 replications):
- nohup ./run/execPrediction.sh Bookcrossing 1 3 1 15 > Bookcrossing/out/execPrediction-1-3-1-15.out 2>&1&
 
#### 4. Evaluation measures computation (for 15 replications):
- nohup ./run/execEval.sh Bookcrossing 1 2 1 15 > Bookcrossing/out/execEval-1-2-1-15.out 2>&1&
   
