import os,sys
import glob


def getFeatureFile(alg):
    global scikitFiles
    for file in scikitFiles:
        if file.lower() in alg.lower():
            return file.replace('train', 'test')
    return None


def readBiasedWeigths(file):
    global weightsPath
    file = open('%s%s' % (weightsPath, file), 'r')
    weights = {}
    for line in file:
        if 'null' in line: print('ERROR: null in User Weigths: %s' % line.strip().split(';')[0]); continue
        line = line.strip().split(';')
        id = int(line[0])
        w = line[1].split()
        weights[id] = [ float(x) for x in w ]
    return weights


def readNotBiasedWeigths(file):
    weightsFile = open('%s%s' % (weightsPath, file), 'r')
    weights = [ float(x) for x in weightsFile.readline().strip().split(' ') ]
    weightsFile.close()
    return weights


def computeBiasedScore(features, keys, weights):
    key = keys.strip()
    user = int(key.split(',')[0])
    if user not in weights:
        print('ERROR: User not found: %d' % user)
        return None
    else:
        w = weights[user]
    score = 0
    for f, feature in enumerate(features):
        score += float(feature.split(':')[1]) * w[f]
    return score


def computeNotBiasedScore(features, weights):
    score = 0
    for f, feature in enumerate(features):
        score += float(feature.split(':')[1]) * weights[f]
    return score


def processPredictions(file):
    print("Processing: %s" % (file))
    global weightsPath, predictionDir, predictionFile, solutionFile, featuresPath, keysFileName, isBiased

    weights = readBiasedWeigths(file) if isBiased else readNotBiasedWeigths(file)
    outFileName = os.path.basename(file).replace('.csv', '')
    if '-train-' in outFileName:
        outFileName += '_HR' if 'HR' in outFileName else '_FWLS' if 'FWLS' in outFileName else '_STREAM' if 'STREAM' in outFileName else '_None'
        outFileName = outFileName[outFileName.find('-train-')+7:]
    outfile = open('%sMO_%s.txt' % (predictionDir, outFileName), 'w')
    featuresFile = getFeatureFile(file)
    if featuresFile is None:
        print('- Features File not found.')
        return
    print('- Features File: %s' % featuresFile)
    featuresFile = open('%s%s.skl' % (featuresPath, featuresFile), 'r')
    keysFile = open('%s%s' % (featuresPath, keysFileName), 'r')

    for features, keys in zip(featuresFile, keysFile):
        features = features.strip().split()[1:]
        score = computeBiasedScore(features, keys, weights) if isBiased else computeNotBiasedScore(features, weights)
        if score is None: continue
        outfile.write('%s,%.6f\n' % (keys.strip(), score))
    outfile.close()


if __name__ == '__main__':
    print("Begin")
    homePath       = sys.argv[1]
    weightsPath    = homePath + sys.argv[2]
    solutionFile   = sys.argv[3]
    featuresPath   = homePath + sys.argv[4]
    keysFileName   = sys.argv[5]
    predictionDir  = homePath + sys.argv[6]
    predictionFile = sys.argv[7]
    isBiased       = sys.argv[8].lower() in [ 'true', 't', 'yes', 'y', '1' ]

    try:
        if not os.path.exists(predictionDir): os.makedirs(predictionDir)
    except Exception:
        pass

    scikitFiles = glob.glob('%s*-train.skl' % (featuresPath))
    scikitFiles = [ os.path.basename(s).replace('.skl', '') for s in scikitFiles ]
    files = [ s for s in os.listdir(weightsPath) if (solutionFile in s) ]

    for file in files:
        processPredictions(file)

    print("End")
