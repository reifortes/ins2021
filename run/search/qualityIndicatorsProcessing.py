import sys, os
import util
import glob
from scipy.spatial import distance
import math

def readWeights(biasFile):
    weights = []
    file = open(biasFile, 'r')
    for line in file:
        if 'nan' in line: continue
        line = line.strip().split()
        #id = int(line[0])
        f1 = float(line[1])
        eild = float(line[2])
        epc = float(line[3])
        s = f1 + eild + epc
        weights.append([ eild/s, epc/s, f1/s ])
    file.close()
    return weights


def readParetoFile(fileName):
    file = open(fileName, 'r')
    points = []
    for line in file:
        if 'nan' in line: continue
        line = line.strip().replace(',', '.').split()
        metrics = [float(x) for x in line]
        eild = metrics[0]
        epc = metrics[1]
        f1 = metrics[2]
        sum = eild + epc + f1
        try:
            points.append([ eild / sum, epc / sum, f1 / sum ])
        except:
            continue
    return points


def computeGD(approx, front):
    sum = 0
    for a in approx:
        min = float('inf')
        for f in front:
            dist = distance.euclidean(a, f)
            if dist < min: min = dist
        sum += min ** 2
    return math.sqrt(sum) / len(approx)


def computeIGD(approx, front):
    return computeGD(front, approx)


if __name__ == '__main__':
    print('Parameters: ' + str(sys.argv))
    util.using("Begin")
    homePath = sys.argv[1]
    solutionsPath = '%s/%s' % (homePath, sys.argv[2])
    biasFile = '%s/BD/%s' % (homePath, sys.argv[3])
    outFileName = '%s/%s' % (solutionsPath, sys.argv[4])
    outFile = open(outFileName, 'w')
    outFile.write('Solution;IGD;\n')
    algorithms = glob.glob('%s/*-FUN.tsv' % solutionsPath)
    weightsDDM = readWeights(biasFile)
    for alg in algorithms:
        if '-seq_' in alg: continue
        solution = os.path.basename(alg)[:-8]
        approx = readParetoFile(alg)
        igd = computeIGD(approx, weightsDDM)
        outFile.write('%s;%s;\n' % (solution, igd))
    outFile.close()
    util.using("End")